//
//  FavouritesViewController.swift
//  Joshua E Smith - FW iOS Giphy Sample App
//
//  Created by Joshua Smith on 2020-12-10.
//

import UIKit
import GiphyUISDK
import GiphyCoreSDK

class FavouritesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    let gifCollectionCellReuseId = "gifCollectionCell"
    var favouriteGiphyURLs = Array<URL>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: "CollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: gifCollectionCellReuseId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        favouriteGiphyURLs = FavouritesController.getURLs() ?? []
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favouriteGiphyURLs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: gifCollectionCellReuseId, for: indexPath)
        guard let gifCell = cell as? CollectionViewCell else {
            return cell
        }
        gifCell.removedFavouriteCallback = {
            self.favouriteGiphyURLs = FavouritesController.getURLs() ?? []
            self.collectionView.performBatchUpdates {
                guard let indexPath = self.collectionView.indexPath(for: gifCell) else {return}
                self.collectionView.deleteItems(at: [indexPath])
            }
        }
        gifCell.showLoading(index: indexPath.item)
        DispatchQueue.global().async {
            guard
                let data = try? Data(contentsOf: self.favouriteGiphyURLs[indexPath.item]),
                let image = GiphyYYImage.init(data: data)
            else {
                return
            }
            DispatchQueue.main.async {
                gifCell.set(image: image, giphyID: self.favouriteGiphyURLs[indexPath.item].lastPathComponent, forIndex: indexPath.item)
            }
        }
        return gifCell
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
