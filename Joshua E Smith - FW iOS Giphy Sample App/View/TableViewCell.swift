//
//  TableViewCell.swift
//  Joshua E Smith - FW iOS Giphy Sample App
//
//  Created by Joshua Smith on 2020-12-10.
//

import UIKit
import GiphyUISDK
import GiphyCoreSDK

class TableViewCell: UITableViewCell {

    @IBOutlet weak var gifImageView: GiphyYYAnimatedImageView!
    @IBOutlet weak var favouriteButton: UIButton!
    var isFavourite: Bool = false
    var giphyID: String?
    private var index: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func toggleFavourite(_ sender: Any) {
        guard let giphyID = giphyID else {
            return
        }
        if isFavourite {
            guard FavouritesController.remove(giphyID: giphyID) else {
                return
            }
        } else {
            guard
                let image = gifImageView.image as? GiphyYYImage,
                let data = image.animatedImageData,
                FavouritesController.add(imageData: data, giphyID: giphyID)
            else {
                return
            }
        }
        
        isFavourite = !isFavourite
        let heartImage = UIImage(systemName: (isFavourite ? "heart.fill" : "heart"))
        favouriteButton.setImage(heartImage, for: .normal)
    }
    
    func set(image: UIImage, giphyID id: String, forIndex imageIndex: Int) {
        // the index check allows this cell to ignore updates that were from previous threads, only allowing the correct image for this row to actually be displayed
        guard imageIndex == index else {
            return
        }
        gifImageView.contentMode = .scaleAspectFit
        gifImageView.image = image
        if !gifImageView.currentIsPlayingAnimation {
            gifImageView.startAnimating()
        }
        giphyID = id
        isFavourite = FavouritesController.getGiphyIDs().contains(id)
        let heartImage = UIImage(systemName: (isFavourite ? "heart.fill" : "heart"))
        favouriteButton.setImage(heartImage, for: .normal)
    }
    
    func showLoading(index: Int) {
        // the index check allows this cell to show the loading image only if the correct image is not already showing
        guard index != self.index else {
            return
        }
        self.index = index
        guard let loadingImage = GiphyYYImage(named: "Elipsis")
        else {
            return
        }
        gifImageView.contentMode = .center
        gifImageView.image = loadingImage
        if !gifImageView.currentIsPlayingAnimation {
            gifImageView.startAnimating()
        }
    }
    
}
