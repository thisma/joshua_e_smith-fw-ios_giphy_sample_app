//
//  FeedViewController.swift
//  Joshua E Smith - FW iOS Giphy Sample App
//
//  Created by Joshua Smith on 2020-12-10.
//

import UIKit
import GiphyUISDK
import GiphyCoreSDK

class FeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var loadingImageView: GiphyYYAnimatedImageView!
    let gifTableCellReuseId = "gifTableCell"
    var gifDatas = Array<GIFObject>()
    var searchTimmer: Timer?
    var searchTerm: String?
    let loadNextPageSemaphore = DispatchSemaphore(value: 1) // only let one Giphy load and model update ( loadNextPage() ) happen at a time, otherwise a race condition can cause tha app to crash.

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "TableViewCell", bundle: Bundle.main), forCellReuseIdentifier: gifTableCellReuseId)
        loadingImageView.image = GiphyYYImage(named: "Elipsis")
        loadGifResultsToTableView()
    }
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData() // simple and efficient way to update the favourite status of the cells on screen after adding them from the Favourites View
    }
    
    func loadGifResultsToTableView() {
        if self.gifDatas.count == 0 {
            // (self.gifDatas.count == 0) means this is a full data reload, so show the loading animation
            loadingImageView.isHidden = false
            if !loadingImageView.currentIsPlayingAnimation {
                loadingImageView.startAnimating()
            }
        }
        DispatchQueue.global().async {
            self.loadNextPageSemaphore.wait()
            defer {
                self.loadNextPageSemaphore.signal()
                // makes sure the loading screen is not present after the data is finished loading
                DispatchQueue.main.sync {
                    self.loadingImageView.isHidden = true
                    if self.loadingImageView.currentIsPlayingAnimation {
                        self.loadingImageView.stopAnimating()
                    }
                }
            }
            let results: ResultsPage?
            if let searchTerm = self.searchTerm {
                results = GiphyRequestController.searchResults(searchTerm: searchTerm, fromIndex: self.gifDatas.count)
            } else {
                results = GiphyRequestController.trendingResults(fromIndex: self.gifDatas.count)
            }
            guard let gifObjects = results else {
                return
            }
            let firstNewRow = self.gifDatas.count
            self.gifDatas.append(contentsOf: gifObjects.data)
            DispatchQueue.main.sync { // this must be synchronous for the defered statements to run at the correct time
                if firstNewRow == 0 {
                    // we are loading an entirely new data set, no insertRows necessary
                    self.tableView.reloadData()
                } else {
                    var newIndexPaths = Array<IndexPath>()
                    for i in firstNewRow ..< self.gifDatas.count {
                        newIndexPaths.append(IndexPath(row: i, section: 0))
                    }
                    self.tableView.performBatchUpdates {
                        self.tableView.insertRows(at: newIndexPaths, with: .automatic)
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gifDatas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: gifTableCellReuseId, for: indexPath)
        guard let gifCell = cell as? TableViewCell else {
            return cell
        }
        gifCell.showLoading(index: indexPath.row)
        DispatchQueue.global().async {
            let gifObject = self.gifDatas[indexPath.row]
            let imageURLString = gifObject.images.downsized.url
            guard
                let url = URL(string: imageURLString),
                let data = try? Data(contentsOf: url),
                let image = GiphyYYImage.init(data: data)
            else {
                return
            }
            DispatchQueue.main.async {
                gifCell.set(image: image, giphyID: gifObject.id, forIndex: indexPath.row)
            }
        }
        return gifCell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let reloadThreshold = 10
        if indexPath.row > gifDatas.count-reloadThreshold {
            loadGifResultsToTableView()
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchTimmer?.invalidate()
        let timer = Timer.scheduledTimer(withTimeInterval: 0.4, repeats: false) { (thisTimer) in
            self.search()
        }
        searchTimmer = timer
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        search()
    }
    func search() {
        self.tableView.setContentOffset(.zero, animated: false)
        if searchBar.text?.count == 0 {
            self.searchTerm = nil
        } else {
            self.searchTerm = searchBar.text
        }
        self.gifDatas = []
        self.tableView.reloadData()
        self.loadGifResultsToTableView()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
