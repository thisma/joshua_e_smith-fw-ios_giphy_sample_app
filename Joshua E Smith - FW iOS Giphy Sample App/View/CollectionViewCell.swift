//
//  CollectionViewCell.swift
//  Joshua E Smith - FW iOS Giphy Sample App
//
//  Created by Joshua Smith on 2020-12-10.
//

import UIKit
import GiphyUISDK
import GiphyCoreSDK

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var gifImageView: GiphyYYAnimatedImageView!
    @IBOutlet weak var favouriteButton: UIButton!

    var isFavourite: Bool = true
    var giphyID: String?
    var removedFavouriteCallback: () -> () = {}
    var timer: Timer?
    
    private var index: Int?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func toggleFavourite(_ sender: Any) {
        guard
            let giphyID = giphyID,
            FavouritesController.remove(giphyID: giphyID)
        else {
            return
        }
        
        isFavourite = !isFavourite
        let heartImage = UIImage(systemName: (isFavourite ? "heart.fill" : "heart"))
        favouriteButton.setImage(heartImage, for: .normal)
        timer = Timer.scheduledTimer(withTimeInterval: 0.4, repeats: false) { (timer) in
            self.removedFavouriteCallback()
        }
    }
    
    func set(image: UIImage, giphyID id: String, forIndex imageIndex: Int) {
        // the index check allows this cell to ignore updates that were from previous threads, only allowing the correct image for this item to actually be displayed
        guard imageIndex == index else {
            return
        }
        gifImageView.contentMode = .scaleAspectFit
        gifImageView.image = image
        if !gifImageView.currentIsPlayingAnimation {
            gifImageView.startAnimating()
        }
        giphyID = id
        isFavourite = FavouritesController.getGiphyIDs().contains(id)
        let heartImage = UIImage(systemName: (isFavourite ? "heart.fill" : "heart"))
        favouriteButton.setImage(heartImage, for: .normal)
    }
    
    func showLoading(index: Int) {
        // the index check allows this cell to show the loading image only if the correct image is not already showing
        guard index != self.index else {
            return
        }
        self.index = index
        guard let loadingImage = GiphyYYImage(named: "Elipsis")
        else {
            return
        }
        gifImageView.contentMode = .center
        gifImageView.image = loadingImage
        if !gifImageView.currentIsPlayingAnimation {
            gifImageView.startAnimating()
        }
    }

}
