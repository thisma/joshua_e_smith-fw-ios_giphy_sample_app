//
//  Pagination.swift
//  Joshua E Smith - FW iOS Giphy Sample App
//
//  Created by Joshua Smith on 2020-12-10.
//

import Foundation

struct Pagination: Decodable {
    let offset: Int
    let totalCount: Int
    let count: Int
}
