//
//  GIFObject.swift
//  Joshua E Smith - FW iOS Giphy Sample App
//
//  Created by Joshua Smith on 2020-12-10.
//

import Foundation

struct GIFObject: Decodable {
    struct Images: Decodable {
        let fixedWidth: Image
        let downsized: Image
    }
    struct Image: Decodable {
        let url: String
        let width: String
        let height: String
        
    }
    let id: String
    let url: String
    let embedUrl: String
    let title: String
    let images: Images
}
