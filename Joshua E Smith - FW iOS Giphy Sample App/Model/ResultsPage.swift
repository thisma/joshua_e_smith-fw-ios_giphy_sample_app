//
//  TrendingPage.swift
//  Joshua E Smith - FW iOS Giphy Sample App
//
//  Created by Joshua Smith on 2020-12-10.
//

import Foundation

struct ResultsPage: Decodable {
    let data: [GIFObject]
    let pagination: Pagination
}
