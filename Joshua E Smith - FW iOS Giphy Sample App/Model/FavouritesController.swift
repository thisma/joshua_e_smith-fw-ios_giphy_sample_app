//
//  FavouritesController.swift
//  Joshua E Smith - FW iOS Giphy Sample App
//
//  Created by Joshua Smith on 2020-12-11.
//

import Foundation
import GiphyUISDK
import GiphyCoreSDK

class FavouritesController {
    
    static func add(imageData: Data, giphyID: String) -> Bool {
        let filemanager = FileManager.default
        guard let documentDirectoryURL = filemanager.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return false
        }
        let urlPath = documentDirectoryURL.appendingPathComponent(giphyID)
        do {
            try imageData.write(to: urlPath)
        } catch {
            return false
        }
        return true
    }
    
    static func getURLs() -> [URL]? {
        let filemanager = FileManager.default
        guard let documentDirectoryURL = filemanager.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return nil
        }
        do {
            let fileURLs = try filemanager.contentsOfDirectory(at: documentDirectoryURL, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
            return fileURLs
        } catch {
            return nil
        }
    }
    
    static func getGiphyIDs() -> [String] {
        guard let urls = getURLs() else { return [] }
        var ids = Array<String>()
        for url in urls {
            ids.append(url.lastPathComponent)
        }
        return ids
    }
    
    static func remove(giphyID: String) -> Bool {
        let filemanager = FileManager.default
        guard let documentDirectoryURL = filemanager.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return false
        }
        let urlPath = documentDirectoryURL.appendingPathComponent(giphyID)
        do {
            try filemanager.removeItem(at: urlPath)
            return true
        } catch {
            return false
        }
    }
}
