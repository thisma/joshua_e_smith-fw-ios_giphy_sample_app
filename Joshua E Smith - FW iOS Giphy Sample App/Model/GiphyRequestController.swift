//
//  GiphyRequestController.swift
//  Joshua E Smith - FW iOS Giphy Sample App
//
//  Created by Joshua Smith on 2020-12-10.
//

import Foundation

class GiphyRequestController {
    static let apiKey = "nvx2BNDtFl9oDxMBaLH9rmfU93RjUE2M"

    static func trendingResults(fromIndex: Int = 0) -> ResultsPage? {
        var urlComponents = URLComponents(string: "https://api.giphy.com/v1/gifs/trending")
        urlComponents?.queryItems = [
            URLQueryItem(name: "api_key", value: apiKey),
            URLQueryItem(name: "offset", value: "\(fromIndex)"),
            URLQueryItem(name: "rating", value: "g")
        ]
        guard let url = urlComponents?.url
        else {
            return nil
        }
        let responseBody = try? String(contentsOf: url)
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        guard
            let data = responseBody?.data(using: .utf8),
            let trendingPage = try? decoder.decode(ResultsPage.self, from: data)
        else {
            return nil
        }
        return trendingPage
    }
    
    static func searchResults(searchTerm: String, fromIndex: Int = 0) -> ResultsPage? {
        var urlComponents = URLComponents(string: "https://api.giphy.com/v1/gifs/search")
        urlComponents?.queryItems = [
            URLQueryItem(name: "api_key", value: apiKey),
            URLQueryItem(name: "offset", value: "\(fromIndex)"),
            URLQueryItem(name: "rating", value: "g"),
            URLQueryItem(name: "q", value: searchTerm)
        ]
        guard let url = urlComponents?.url
        else {
            return nil
        }
        let responseBody = try? String(contentsOf: url)
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        guard
            let data = responseBody?.data(using: .utf8),
            let trendingPage = try? decoder.decode(ResultsPage.self, from: data)
        else {
            return nil
        }
        return trendingPage
    }
}
