= iOS Giphy Sample App =
Please open the xcworkspace file, as it contains the dependancy required for animated gifs.

For displaying the animated gifs I used Giphy’s SDK framework installed via CocoaPods. Using Giphy’s framework has the benefit that if any Giphy specific features would be desired in the future everything is already setup for their inclusion. Additionally since this project relies on Giphy, using their own framework rather than a third party’s framework reduces the number of dependancies that would need to be maintained.

My development time for this app was about 12 to 13 hours.


== Bonus ==
Infinite Scrolling Pagination:
I did implement pagination with infinite scrolling. Including infinite scrolling in an app with this kind of content is exclusively better; as any other behaviour will distract from the app’s primary feature. Additionally, knowing how to overcome the various difficulties, and find and mitigate the potential edge case problems with large table and collection views are foundational skills required for an iOS developer to be productive on projects and helpful on a team. A simple example is keeping the IO/network tasks off the main thread to allow for smooth scrolling, and then mitigating the many potential pitfalls of multi-threading.

RxSwift:
I did not use RxSwift in this project. My development strategy includes a tendency toward being a medium to late-term adopter of brand new third party technologies. After years of experience learning new technologies, I have found primarily using the platform’s tools, secondarily including tools and assets which are already ubiquitous in the industry, and including new or obscure tools only as a tertiary approach yields the most robust, complete, extensible, and maintainable solution in the shortest time. RxSwift is a good example of some of the reasons for this approach; RxSwift is fading into obscurity as development on Swift continues to incorporate features which match or exceed those RxSwift includes. Having an excellent grasp on fundamental skills and first principles allows even more rapid development than using auxiliary tools.

MVVM:
I have begun using SwiftUI in the programming projects which I develop exclusively for my personal use. This is in line with my professional development strategy of having a secondary focus on the latest technologies included with a platform. So I am fully prepared to include it in professional projects, when the required threshold of SwiftUI’s availability on consumer’s devices is met for a project. Using both MVC and MVVM in a single project of this size is impractical, and I believe It is much more important that I show my proficiency with MVC, as certainly it will be the best available option for most projects for some time.


== Project Organization ==
This project has the View and View-Controllers in the View group/folder and the Model and Model-Controllers in the Model group/folder.

The network communications are contained exclusively in the GiphyRequestController, which creates the model objects as requested, and Foundation’s Data class.

The File handling for keeping the favourites images stored locally is handled by FavouritesController.

ResultsPage, Pagination, and GIFObject are the model classes which reflect the data that is retrieved using the Giphy API. Only the parts of the data that appeared to be relevant to this project are included. There are many more options and data that could be easily added for features in future iterations.

TableView and CollectionView are nearly identical. The project could be simplified a bit by merging the identical code. I would, however, expect that as the project is developed they would diverge more. So I’d be inclined to leave them separate even though its not DRY.


== Documentation ==
This file, of course, contains some documentation for the code of this project. There are also a few comments in the code. I find that documenting code by liberally applying comments often quickly becomes misleading because of outdated comments, possibly because comments are code that never runs. To combat this issue with code documentation I work to write my code for three people/entities: first the poor person who has to look at my code to figure out how to update or fix it, second the computer, and third me in the future. So my comments will not often explain what the code is doing, but rather why it is. For example a comment might explain how some code that may seem excessive prevents a crash, or describe the way a previous attempted approach that seems better at first actually fails. For what code should be doing, when that may not be obvious, I believe tests are a better documentation strategy because they are actually live working code.