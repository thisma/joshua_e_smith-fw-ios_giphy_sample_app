//___FILEHEADER___

import XCTest
@testable import Joshua_E_Smith___FW_iOS_Giphy_Sample_App

class ___FILEBASENAMEASIDENTIFIER___: XCTestCase {
    
    override func setUp() {
    }
    
    override func tearDown() {
    }
    
    func test_gif_data_can_be_retrieved_from_giphy() {
        let results1 = GiphyRequestController.trendingResults()
        XCTAssertNotNil(results1)
        XCTAssertGreaterThan(results1!.data.count, 0)
        let results2 = GiphyRequestController.trendingResults(fromIndex: results1!.data.count)
        XCTAssertNotNil(results2)
        XCTAssertGreaterThan(results2!.data.count, 0)
        let results3 = GiphyRequestController.searchResults(searchTerm: "test")
        XCTAssertNotNil(results3)
        XCTAssertGreaterThan(results3!.data.count, 0)
        let results4 = GiphyRequestController.searchResults(searchTerm: "test", fromIndex: results3!.data.count)
        XCTAssertNotNil(results4)
        XCTAssertGreaterThan(results4!.data.count, 0)
        
        XCTAssertGreaterThan(results1!.data.first!.images.downsized.url.count, 0)
        XCTAssertGreaterThan(results1!.data.last!.images.downsized.url.count, 0)
        XCTAssertGreaterThan(results2!.data.first!.images.downsized.url.count, 0)
        XCTAssertGreaterThan(results2!.data.last!.images.downsized.url.count, 0)
    }
    
    func test_consecutive_results_are_properly_alligned_with_no_overlap() {
        let results1 = GiphyRequestController.trendingResults()
        let improperlyAllignedResult1 = GiphyRequestController.trendingResults(fromIndex: results1!.data.count-1)
        let properlyAllignedResult1 = GiphyRequestController.trendingResults(fromIndex: results1!.data.count)
        XCTAssertEqual(results1!.data.last!.id, improperlyAllignedResult1!.data.first!.id)
        XCTAssertNotEqual(results1!.data.first!.id, properlyAllignedResult1!.data.last!.id)
        
        let results2 = GiphyRequestController.searchResults(searchTerm: "testing")
        let improperlyAllignedResult2 = GiphyRequestController.searchResults(searchTerm: "testing",fromIndex: results2!.data.count-1)
        let properlyAllignedResult2 = GiphyRequestController.searchResults(searchTerm: "testing",fromIndex: results2!.data.count)
        XCTAssertEqual(results2!.data.last!.id, improperlyAllignedResult2!.data.first!.id)
        XCTAssertNotEqual(results2!.data.first!.id, properlyAllignedResult2!.data.last!.id)
    }
    
    func test_favourite_save_retrieve_and_delete() {
        let results1 = GiphyRequestController.trendingResults()
        let giphyID = results1!.data.first!.id
        let imageURLString = results1!.data.first!.images.downsized.url
        let url = URL(string: imageURLString)
        XCTAssertNotNil(url)
        let data = try! Data(contentsOf: url!)
        XCTAssertNotNil(data)
        // if the giphy file is already on the device these tests will fail, so remove it, and then put it back at the end of the test
        let giphyPreexists = FavouritesController.getGiphyIDs().contains(giphyID)
        if giphyPreexists {
            XCTAssert(FavouritesController.remove(giphyID: giphyID))
        }
        XCTAssertFalse(FavouritesController.getGiphyIDs().contains(giphyID))
        XCTAssert(FavouritesController.add(imageData: data, giphyID: giphyID))
        XCTAssert(FavouritesController.getGiphyIDs().contains(giphyID))
        XCTAssert(FavouritesController.remove(giphyID: giphyID))
        XCTAssertFalse(FavouritesController.getGiphyIDs().contains(giphyID))
        if giphyPreexists {
            XCTAssert(FavouritesController.add(imageData: data, giphyID: giphyID))
        }
    }


}
